import resolve from '@rollup/plugin-node-resolve';
import commonjs from '@rollup/plugin-commonjs';

export default {
  input: 'web_src/scripts.js',
  output: {
    format: 'iife'
  },
  name: 'Clientcare',
  plugins: [
    resolve(),
    commonjs()
  ]
};