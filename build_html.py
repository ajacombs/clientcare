import subprocess
import json
import logging
import shutil
from typing import List, Callable
import os
import argparse
import gzip

from lxml import etree
from lxml.builder import E

import clientcare


def run_task(
        name: str, args: List[str],
        input_: str = None, outfile: str = None,
        error_func: Callable[[str], str] = None
        ) -> str:
    """
    Run the specified task in a subprocess and return its output.

    name: The name of the task being run, used for logging.
    args: A list whose first element is the executable to run, with additional
        elements containing any arguments.
    input_: Optional string to pass to stdin of the subprocess.
    outfile: Optional file which the subprocess outputs to if it does not
        output to stdout. The contents will be read, and the file deleted.
    error_func: Optional function to be called to generate the error message
        to be logged. Will be passed stderr, and must return a string.
    """
    try:
        task = subprocess.run(
            args, input=input_,
            stdout=subprocess.PIPE, stderr=subprocess.PIPE, encoding='utf8')
        task.check_returncode()
        if outfile is not None:
            with open(outfile, encoding='utf8') as outfile_f:
                output = outfile_f.read()
            os.unlink(outfile)
        else:
            output = task.stdout
        logger.info(f'{name} successful')
        return output
    except subprocess.CalledProcessError as error:
        if error_func is not None:
            message = error_func(error.stderr)
        else:
            message = error.stderr
        logger.error(f'{name} failed with error:\n\n{message}')


if __name__ == '__main__':
    # Parse arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('-m', '--minify', action='store_true')
    minify = parser.parse_args().minify

    # Setup logging
    logger = logging.getLogger()
    handler = logging.StreamHandler()
    formatter = logging.Formatter(
        '{asctime}  {levelname:8} {message}', '%Y-%m-%d %H:%M:%S', style='{')
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    logger.setLevel(logging.DEBUG)

    # Build html
    document_contents, context = clientcare.build_document_contents(
        input_file_path='./document_src/'
                        'Lawyers Conduct and Client Care Rules [formatted].xml',
        context=clientcare.Context(mode=clientcare.Mode.html))
    toc, context = clientcare.build_html_table_of_contents(context)
    body_els = E.article(
        {'class': 'regulation__body'},
        *document_contents,
        E.ol(*context.footnotes, {'class': 'footnotes'}))
    body = etree.tostring(
        body_els,
        method='xml',
        encoding='unicode',
        pretty_print=True)

    # Build assets
    NODE = shutil.which('node')
    css = run_task(
        name='CSS compilation',
        args=[
            NODE,
            './node_modules/node-sass/bin/node-sass',
            './web_src/styles.scss'],
        error_func=lambda e: json.loads(e)['formatted'])
    js = run_task(
        name='JS compilation',
        args=[
            NODE,
            './node_modules/rollup/dist/bin/rollup',
            '--config', 'rollup.config.js'])
    if minify:
        css = run_task(
            name='CSS minification',
            args=[
                NODE,
                './node_modules/clean-css-cli/bin/cleancss'],
            input_=css)
        js = run_task(
            'JS minification',
            args=[
                NODE,
                './node_modules/uglify-js/bin/uglifyjs',
                '--compress', '--mangle', '--toplevel'],
            input_=js)

    # Build final file
    with open('./web_src/html-template.html', encoding='utf8') as template_f:
        template = template_f.read()
    processed = template.format(
        styles=css.replace('\n', '\n    '),
        scripts=js.replace('\n', '\n    '),
        intro='',
        body=body.replace('\n', '\n      '),
        toc=toc.replace('\n', '\n      '))
    gzipped = gzip.compress(processed.encode('utf8'))
    with open('./public/index.html', 'w', encoding='utf8') as html_f:
        html_f.write(processed)
    with open('./public/index.html.gz', 'wb') as gzip_f:
        gzip_f.write(gzipped)
    logger.info('HTML compilation successful')
