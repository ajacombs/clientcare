﻿//@target indesign

var doc = app.documents.item(0);

app.findTextPreferences = null;
app.changeTextPreferences = null;
app.findGrepPreferences = null;
app.changeGrepPreferences = null;

app.findGrepPreferences.findWhat = "__FOOTNOTE_\\d+__";

var numberOfFootnotes = doc.findGrep().length / 2
var palette = new Window('palette', 'Moving footnotes into place');
palette.label = palette.add('statictext', undefined, '');
palette.label.preferredSize.width = 300;
palette.pbar = palette.add('progressbar', undefined, 0, numberOfFootnotes);
palette.pbar.preferredSize.width = 300;
palette.show();

for (var i=1; i < numberOfFootnotes+1; i++) {

  app.findTextPreferences.findWhat = "__FOOTNOTE_" + i + "__";
  var finds = doc.findText();
  var footnoteMarker = finds[0].insertionPoints.item(0);
  var footnoteText = finds[1].paragraphs.item(0);
  palette.label.text = 'Moving footnote ' + (i) + ' of ' + numberOfFootnotes;
  var footnote = footnoteMarker.footnotes.add();
  footnoteText.move(LocationOptions.AT_END, footnote.paragraphs.item(0));
  doc.changeText();
  palette.pbar.value = i + 1;

}


app.findGrepPreferences.findWhat = '~b$';
app.findGrepPreferences.appliedParagraphStyle = app.activeDocument.footnoteOptions.footnoteTextStyle;
app.activeDocument.changeGrep();

app.findGrepPreferences = null;
app.findTextPreferences = null;
