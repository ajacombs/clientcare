rules? (\d[A-Za-z.()\d]+[A-Za-z)\d])


rule 5.4, 5.5, or 5.6
rule 2.8
rule 2.8.1
rule 3.4 or 3.5
rule 3.4A or 3.5A
rules 3.4 and 3.5
rules 14.5.1 and 14.5.2
rule 14.5.2(h)
rule 14.2(b).
rule 16.7 or 16.8
Rules 16.3 to 16.5
rule 15.2.4(a) and (b)
rules 3.1, 3.3, and 3.4.
rules 14.5.2(d) to (h)
rules 14.5.2(d) to (i)
rules 14.5.2(f) and (g))
Rules 7.1 to 7.6
rule 3.4A or 3.5A

Rules 3.4, 3.4A, 3.5 and 3.5A

First part of ref:
x.x
x.xA
x.x.x

Subsequent parts:
(a)


rule