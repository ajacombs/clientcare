<!-- ................................................................................. -->
<!-- NZ Supplementary Order Paper Document Type Definition ........................... -->
<!-- File nzsop.dtd .................................................................. -->

<!-- ................................................................................. -->
<!--

     This file is one of a set of files comprising the DTDs developed
     for the New Zealand Parliamentary Counsel Office.

     Please see the DTD Guide for implementation and usage details.

-->
<!-- ................................................................................. -->
<!--
     This is the driver file for the NZ Supplementary Order Paper (SOP) DTD.
     Please use the following formal public identifier to identify it:

         "-//NZPCO//DTD SOP//EN"

     For example, if your document's top-level element is sop, and you are using
     the NZ SOP DTD directly, use the FPI in the DOCTYPE declaration:

         <!DOCTYPE sop PUBLIC "-//NZPCO//DTD SOP//EN"
                              "nzsop/nzsop.dtd">
         <sop>
         ...
-->
<!-- ................................................................................. -->

<!-- ................................................................................. -->
<!-- Document History ................................................................ -->
<!--

{Initials} {Date YYYY-MM-DD} {Comment}

   2002-05-31          Sign-off by New Zealand Parliamentary Counsel Office.
                       This file formalised as version 1.0
   2002-10-25          version 1.1
   2002-08-10 03:19PM  version 1.2
   2002-10-01          v2.0 Standardise all DTD versions to 2.0
   2002-10-12 09:07AM  version 2.1
   2002-10-28 08:59AM  v2.2
   2002-11-06 05:23PM  v2.3
   2002-11-14 05:27PM  v2.4
   2002-11-30 02:41PM  v2.5
   2002-12-27 07:48AM  v2.6
   2003-01-18 03:36PM  v2.7
   2003-02-05 11:03AM  v2.8
   2003-02-28 10:08AM  v2.9

**** PAL PHASE 3 ****
   2005-03-31 02:35PM  v3.2
   2005-05-23 03:19PM  v3.3
   2005-09-19 05:26PM  v3.3.1
   2005-10-17 04:43PM  v3.3.2
   2005-11-07 07:54AM  v4.0
   ...
   2007-09-02          v4.6
   2007-12-06          v5.0 - FINAL version before system implementation
   ...
   2010-08-10          v5.4
   2011-01-07          v5.5

     ................................................................................. -->

<!-- ................................................................................. -->
<!--
     Change History Since March 2005

AB 03/27/2005 05:46PM  added footer attribute to element sop
AB 03/30/2005 06:44PM  added %lang.att; (xml:lang) to sop
AB 03/30/2005 07:14PM  removed redundant redefn of %reference.model;
AB 04/12/2005 11:38AM  added id to sop.part and sop.subpart
AB 04/20/2005 11:09AM  amended insertion.atts
AB 04/20/2005 02:02PM  moved struckoutwords insertwords and option to  Billcore
AB 04/21/2005 09:58AM  implemented struckout by attributes model
AB 05/17/2005 06:08PM  review and reorganisation of metadata
AB 05/23/2005 06:37PM  rewrote subprov.group as subprov.crosshead
AB 05/24/2005 08:31AM  rewrote label-para.group as label-para.crosshead
AB 09/19/2005 03:57PM  removed metablock and updated %sop.model; and %sop.atts;
AB 09/16/2005 04:31PM  added @close.quote to instrument.amend
AB 10/11/2005 02:15PM  added @open.quote to instrument.amend
AB 10/11/2005 02:31PM  added @increment to instrument.amend
AB 11/01/2005 03:55PM  major overhaul to allow change tracking in Acts and Regs
MB 16/06/2006 10:00AM  enumeration for @stage revised, @terminated added
MB 27/09/2006          %nzcore declaration and reference standardised on all lower case to match actual filename.
MB 02/10/2006          Parameter entity %doctype.local.atts introduced to provide for metadata attributes on
                       chunks as well as root element. Root and chunk attlists and other parameter entity
                       declarations revised to suit.
MB 03/10/2006          Optional @id added to all elements that didn't already have it, %processing.id retired.

**** V 4.6 changes ****
MB 23/01/2007          Declaration of PE %body.model moved to pre-empt new nzcore declaration of the PE.
MB 23/01/2007          Declaration of //body content model revised to add //heading wh. not present moved %body.model.
MB 24/01/2007          Declaration of att @xml:lang added to //sop to override %global declaration of @xml:lang,
                       which is now #IMPLIED.

**** V 5.0 changes ****
MB 12/06/2007          Content model for element sop revised to allow external-document instead of body.

**** V 5.1 changes ****
MB 16/06/2008          None

**** V 5.5 changes ****
MA 2011-01-07          NZCR-154 changes. Includes general comments, whitespace and formatting cleanup.
MA 2012-02-01          Added default XML Namespace to core; added PE to document element of each doctype.

**** LENZ 16 changes ****
DD 2015-03-02          PCODM-1407 changes to use default rather than implied values for stage and use lang value from core.

-->
<!-- ................................................................................. -->

<!-- Dynamic Link Markup ............................................................. -->
<!ENTITY % nzlink.dtd PUBLIC "-//NZPCO//DTD Dynamic Link Markup//EN"
                              "nzlink.dtd">
%nzlink.dtd;

<!-- ................................................................................. -->
<!-- Redefinition of models for SOP's/Slips .......................................... -->

<!-- Duplicate of defn in Core required here -->
<!ENTITY % change.tracked.elements "struckoutwords | insertwords" >

<!-- old
<!ENTITY % basic-phrase "#PCDATA | emphasis | superscript | subscript | fraction" >
-->
<!-- Duplicate of defn in Core required here -->
<!ENTITY % basic-phrase
      "#PCDATA | emphasis | superscript | subscript | fraction | %change.tracked.elements;" >

<!-- redefined from Core to allow changeable -->
<!ENTITY % phrase
      "%basic-phrase;
       | footnote | footnote-ref %dlm-link.model; | field | leader | def-term | citation
       | quote.in | graphic.inline | changeable" >

<!ENTITY % notes.model "((ird.aids | option)*, query-note?)" >

<!-- Declarations to provide for metadata atts on root and chunk elements. -->
<!ENTITY % reprints.officer.att "" >
<!ENTITY % sop-stages "(draft | consultation-draft | published)" >
<!ENTITY % sop-terminated "(superseded | withdrawn)" >
<!ENTITY % act-types "(public | local | private | provincial | imperial)" >
<!ENTITY % reg-types
      "(letterspatent | bylaw | determination | direction | notice | order
       | proclamation | regulation | rule | warrant )" >
<!ENTITY % doctype.local.atts
      "sop.no                  CDATA             #IMPLIED
       pco.suffix              CDATA             #IMPLIED
       raised.by               (Government | Members ) 'Government'
       stage                   %sop-stages;      'draft'
       terminated              %sop-terminated;  #IMPLIED" >

<!ENTITY % body.model
      "motion?, option*,
       ((sop.split, option*)+
       | (sop.amend, option*)+
       | (sop.part, option*)+)" >

<!-- ................................................................................. -->
<!-- Core DTD ........................................................................ -->

<!ENTITY % nzcore PUBLIC "-//NZPCO//DTD Core Elements//EN"
                         "nzcore.dtd" >
%nzcore;

<!-- ................................................................................. -->
<!-- Elements ........................................................................ -->

<!ENTITY % sop.structure
      "option | eqn | figure | graphic | table | list
       | label-para | label-para.crosshead | def-para
       | (instrument.amend, follow-text?) | (quote, follow-text?)" >

<!ENTITY % sop.para.model
      "(text?, ((%sop.structure;), text?)*,
       proviso*, notes?)" >
<!ENTITY % sop.para.atts
      "%standard-statuses.meta.atts;
       %id.att;" >
<!ELEMENT  sop.para  %sop.para.model; >
<!ATTLIST  sop.para
       %sop.para.atts; >

<!ENTITY % sop.model
      "(date?, billref, billref.alt?,
       (external-document | body),
       explnote?)*" >
<!ENTITY % sop.atts
       "%global.atts;
       %doctype.local.atts;" >
<!ELEMENT  sop  %sop.model; >
<!ATTLIST  sop
       %ns.atts;
       %sop.atts;
>

<!ENTITY % body.atts "%id.att;" >
<!ELEMENT  body  (heading, %body.model;) >
<!ATTLIST  body
       %body.atts; >

<!ENTITY % sop.split.model "((para | sop.newenact | option)+)" >
<!ENTITY % sop.split.atts
      "%standard-statuses.meta.atts;
       %id.att;" >
<!ELEMENT  sop.split  %sop.split.model; >
<!ATTLIST  sop.split
       %sop.split.atts; >

<!ENTITY % sop.newenact.model "(enactment, option*, (prov, option*)+)" >
<!ENTITY % sop.newenact.atts
      "%standard-statuses.meta.atts;
       %id.att;" >
<!ELEMENT  sop.newenact  %sop.newenact.model; >
<!ATTLIST  sop.newenact
       %sop.newenact.atts; >

<!ENTITY % sop.amend.model "(clause.ref+, (sop.para | option)+)" >
<!ENTITY % sop.amend.atts
      "%id.att;
       %standard-statuses.meta.atts;" >
<!ELEMENT  sop.amend  %sop.amend.model; >
<!ATTLIST  sop.amend
       %sop.amend.atts; >

<!ENTITY % sop.part.model
      "(label, heading?, option*,
       ((sop.amend, option*)+ |
       (sop.subpart, option*)+))" >
<!ENTITY % sop.part.atts
      "%id.att;
       %standard-statuses.meta.atts;" >
<!ELEMENT  sop.part  %sop.part.model; >
<!ATTLIST  sop.part
       %sop.part.atts; >

<!ENTITY % sop.subpart.model
      "(label?, heading?, option*,
       (sop.amend, option*)+)" >
<!ENTITY % sop.subpart.atts
      "%id.att;
       %standard-statuses.meta.atts;" >
<!ELEMENT  sop.subpart  %sop.subpart.model; >
<!ATTLIST  sop.subpart
       %sop.subpart.atts; >

<!ELEMENT  billref  %title.model; >
<!ATTLIST  billref
       %title.atts; >

<!ELEMENT  billref.alt  %title.model; >
<!ATTLIST  billref.alt
       %title.atts; >

<!ENTITY % motion.model "(%basic-phrase;)*" >
<!ENTITY % motion.atts "%id.att;" >
<!ELEMENT  motion  %motion.model; >
<!ATTLIST  motion
       %motion.atts; >

<!ENTITY % clause.ref.model "(#PCDATA | %change.tracked.elements;)*" >
<!ENTITY % clause.ref.atts "%id.att;" >
<!ELEMENT  clause.ref  %clause.ref.model; >
<!ATTLIST  clause.ref
       %clause.ref.atts; >

<!ENTITY % instrument.amend.model
      "(%amend.model;
       | (amend, follow-text?))" >
<!ENTITY % instrument.amend.atts
      "open.quote              (yes | no)        'no'
       close.quote             (yes | no)        'no'
       increment               (1 | 2| 3)        #IMPLIED
       %standard-statuses.meta.atts;
       %id.att;" >
<!ELEMENT  instrument.amend %instrument.amend.model; >
<!ATTLIST  instrument.amend %instrument.amend.atts; >

<!-- End of NZ Supplementary Order Paper Document Type Definition .................... -->
<!-- ................................................................................. -->
