import FontFaceObserver from 'fontfaceobserver'


function scrollToAnchor(href, offset) {
  var destinationElement = document.getElementById(href.slice(1));
  if(destinationElement) {
    var anchorOffset = destinationElement.offsetTop - offset;
    window.scrollTo(window.pageXOffset, anchorOffset);
  }
}


function chapterHeadingScrollHandler(chapters, body, headerTitle, scrollTimer) { 
 
  if(scrollTimer) { 
    window.clearTimeout(scrollTimer); 
  } 
 
  scrollTimer = window.setTimeout(function() { 
    var windowOffset = body.getBoundingClientRect().top * -1; 
    var chapter, text; 
    for (var i = 0; i < chapters.length; i++) { 
      chapter = chapters[i]; 
      if (chapter.offsetTop > windowOffset) { 
        if (i > 0) { 
          text = chapters[i-1].innerText.replace('\n', ': ') 
        } else { 
          text = 'Rules of Conduct and Client Care'; 
        }; 
        break; 
      } else if (i === chapters.length-1){ 
        text = chapter.innerText.replace('\n', ': '); 
      }; 
    }; 
    headerTitle.innerHTML = text 
  }, 100);
}


function ready(fn) {
  if ( document.attachEvent 
     ? document.readyState === "complete"
     : document.readyState !== "loading"
  ){
    fn();
  } else {
    document.addEventListener('DOMContentLoaded', fn);
  }
}


ready(function() {

  var chapters = document.getElementsByClassName('chapter');
  var body = document.getElementsByTagName('body')[0];
  var headerTitle = document.getElementsByClassName('header__title')[0];
  var scrollTimer;
  window.addEventListener('scroll', function() {
    chapterHeadingScrollHandler(chapters, body, headerTitle, scrollTimer)
  });


  var anchorRegex = /^#[^ ]+$/;
  document.body.addEventListener('click', function(event) {
    var target = event.target;
    var el, href;
    if (target.matches('#menu-button')) {
      el = document.getElementById('menu')
      if ( el.classList.contains('menu--hidden') ) {
        el.classList.remove('menu--hidden');
      } else {
        el.classList.add('menu--hidden');
      }
    } else if (target.matches('.menu-chapter__expand-button')) {
      el = target.parentNode;
      if ( el.classList.contains('active') ) {
        el.classList.remove('active');
      } else {
        el.classList.add('active');
      }
    } else if (target.nodeName === 'A') {
      href = target.getAttribute('href');
    } else if (target.parentNode.nodeName === 'A') {
      href = target.parentNode.getAttribute('href');
    } else {
      el = document.getElementById('menu')
      if ( ! el.classList.contains('menu--hidden') ) {
        el.classList.add('menu--hidden');
      }
    }
    if (href !== undefined) {
      if(anchorRegex.test(href)) {
        event.preventDefault();
        scrollToAnchor(href, 8);
      }
    }
  });

  if(
    anchorRegex.test(window.location.hash)
  ) {
    scrollToAnchor(window.location.hash, 0);
  }

});

var montserrat_300 = new FontFaceObserver('Montserrat', {weight: 300});
var montserrat_500 = new FontFaceObserver('Montserrat', {weight: 500});
var montserrat_700 = new FontFaceObserver('Montserrat', {weight: 700});
var merriweather_400 = new FontFaceObserver('Merriweather', {weight: 400});
var merriweather_400_i = new FontFaceObserver('Merriweather', {weight: 400, 'style': 'italic'});
var merriweather_700 = new FontFaceObserver('Merriweather', {weight: 700});


Promise.all([
  montserrat_300.load(null, 5000),
  montserrat_500.load(null, 5000),
  montserrat_700.load(null, 5000),
  merriweather_400.load(null, 5000),
  merriweather_400_i.load(null, 5000),
  merriweather_700.load(null, 5000)
]).then(function () {
  console.log('Fonts have loaded');
  document.documentElement.classList.add('fonts-loaded');
}, function () {
  console.log('Fonts are not available after waiting 3 seconds');
});
