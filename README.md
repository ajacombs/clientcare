# Rules of Conduct and Client Care redesign

Proof of concept for an improved way of presentating legislation: with
mobile-friendly responsive design, improved typography, and in-text navigation.

An example using the Lawyers and Conveyancers Act (Lawyers: Conduct and Client 
Care) Rules 2008 can be seen here: 
[https://ajacombs.gitlab.io/clientcare/](https://ajacombs.gitlab.io/clientcare/)

***Note:*** *The amendments made by Lawyers and Conveyancers Act (Lawyers:
Conduct and Client Care) Amendment Rules 2021 or later amendments have not yet
been incorporated*

[A PDF version made by formatting the processed XML in InDesign is also available.](https://gitlab.com/ajacombs/clientcare/-/raw/master/indesign/Rules%20of%20Conduct%20and%20Client%20Care.pdf?inline=false)
