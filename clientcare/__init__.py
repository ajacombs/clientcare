from enum import Enum
from typing import Union, List, Any, Callable, Dict, Tuple
from collections import OrderedDict

from dataclasses import dataclass, field

from lxml import etree
from lxml.builder import E
from lxml.etree import ElementBase

ListOfElementsOrStrs = List[Union[ElementBase, str]]


class Mode(Enum):
    indd = 'indd'
    html = 'html'


@dataclass
class Context:
    mode: Mode
    footnotes: List[ElementBase] = field(default_factory=list)
    current_footnote_number: int = 0
    table_of_contents: \
        Dict[str, List[Tuple[str, str]]] = field(default_factory=OrderedDict)
    current_chapter_title: str = ''
    current_chapter_number: str = ''


HandlerReturn = Tuple[ListOfElementsOrStrs, Context]


Handlers = Dict[
    str, Callable[
        [ElementBase, Context],
        ListOfElementsOrStrs
    ]
]

HANDLERS: Handlers = {}

INLINE_HANDLERS: Handlers = {}


def handler(elementname: str) -> Callable:
    def wrapper(func: Callable) -> Callable:
        HANDLERS[elementname] = func
        return func
    return wrapper


def inline_handler(elementname: str) -> Callable:
    def wrapper(func: Callable) -> Callable:
        INLINE_HANDLERS[elementname] = func
        return func
    return wrapper


################################################################################
# Element handlers                                                             #
################################################################################


@handler('head1')
def chapter(element: ElementBase, context: Context) -> HandlerReturn:
    chapter_number = etree.tostring(
        element.find('label'), method='text').decode('utf8').strip()
    chapter_title = etree.tostring(
        element.find('heading'), method='text').decode('utf8').strip()
    id_ = chapter_number.lower().replace(' ', '_')
    if context.mode == Mode.indd:
        return [
            E.chapter_number(chapter_number), '\n',
            E.chapter_title(chapter_title), '\n'
        ], context
    elif context.mode == Mode.html:
        context.table_of_contents[chapter_title] = []
        context.current_chapter_title = chapter_title
        context.current_chapter_number = chapter_number.lower()
        return [E.h2(
                    E.span(chapter_number, {'class': 'chapter__number'}),
                    E.br(),
                    E.span(chapter_title, {'class': 'chapter__title'}),
                    {'class': 'chapter', 'id': id_}
                )], context


@handler('prov')
def prov(element: ElementBase, context: Context) -> HandlerReturn:
    heading_el = element.find('heading')
    if heading_el is not None:
        heading_content, context = build_text(heading_el, context)
        if context.mode == Mode.indd:
            return [E.section_head(*heading_content), '\n'], context
        elif context.mode == Mode.html:
            heading_text = heading_content[0]
            para_id = heading_text.lower().replace(" ", "_")
            chapter_number = \
                context.current_chapter_number.lower().replace(" ", "_")
            id_ = f'{chapter_number}:_{para_id}'
            context.table_of_contents[
                context.current_chapter_title
            ].append((heading_text, id_))
            return [
                E.h3(*heading_content, {'class': 'sectionhead', 'id': id_})
            ], context
    else:
        return [], context


@handler('subprov')
@handler('label-para')
@handler('def-para')
def subprov(element: ElementBase, context: Context) -> HandlerReturn:
    # Find label text
    label_el = element.find('label')
    if label_el is not None:
        label = label_el.text
    else:
        label = None

    # Find depth
    depth = get_depth(element)

    numbered_head = element.xpath("para[1]/text/emphasis[@style='bold']")
    is_numbered_head = len(numbered_head) > 0
    multiple_paras = element.xpath("./para")
    has_multiple_paras = len(multiple_paras) > 1

    # inline section head with numbered label
    is_section_head_numbered = bool(
        element.tag == 'subprov'
        and is_numbered_head
        and has_multiple_paras
        and label is not None)
    if is_section_head_numbered:
        heading_content, context = build_text(numbered_head[0], context)
        content, context = build_text(element.xpath('para[2]/text')[0], context)
        if context.mode == Mode.indd:
            return [
                E.section_head_numbered(
                    E.section_number(label), '\t', *heading_content), '\n',
                E(f'section__{depth}', '\t', *content), '\n'], context
        elif context.mode == Mode.html:
            return [
                E.p(
                    E.span(label, {'class': 'sectionhead-numbered__label'}),
                    *heading_content,
                    {'class': 'sectionhead-numbered'}),
                E.p(*content, {'class': f'para para__{depth}'})
            ], context

    content, context = build_text(element.find('./para/text'), context)

    # labelled sections
    if label is not None:
        if context.mode == Mode.indd:
            # wrap in thin-spaced brackets if first character is not a number
            if not label[0].isdigit():
                oversize_length = 5  # max 1 letter + 2 brackets + 2 spaces
                label = f'(\u200A{label}\u200A)'
            else:
                oversize_length = 3  # max 2 numbers and a dot

            if len(label) > oversize_length:
                spacechar = '\u2002'  # En space for long labels
            else:
                spacechar = '\t'  # Tab for short labels
            return [
                E(f'section__{depth}',
                  E.section_number(label), spacechar, *content), '\n'
            ], context
        elif context.mode == Mode.html:
            ref = build_paragraph_reference(label_el)
            if not label[0].isdigit():
                label = f'({label}) '
            else:
                label = f'{label} '
            return [E.p(
                E.span(label, {'class': 'para__label'}),
                *content,
                {'class': f'para para__{depth}', 'id': f'rule_{ref}'}
            )], context
    # Unlabelled sections
    else:
        if context.mode == Mode.indd:
            return [E(f'section__{depth}', '\t', *content), '\n'], context
        elif context.mode == Mode.html:
            return [E.p(*content, {'class': f'para para__{depth}'})], context


@handler('text')
def text(element: ElementBase, context: Context) -> HandlerReturn:
    """
    Handler for <text> elements.

    Generally we don't want to handle text elements, as they are processed
    from the relevant parent elements as we find them. However, if a parent
    has multiple <text> children, we need to handle the latter elements
    themselves, in order to ensure they appear in the right document order.

    We test for elements which have:
    1. A preceding sibling <text>
    2. A parent which is a <para>
    3. A grandparent which is a <subprov>, a <label-para> or a <def-para>
    If the parent of this <text> element is a <proviso>, we instead want to test
    the above conditions using the <proviso> parent.

    If the conditions are met, we return the same formtting as for unlabelled
    paragraphs in the subprov handler. If they are not, we return an empty list.
    """
    parent_el = element.getparent()
    if parent_el.tag == 'proviso':
        test_element = parent_el
    else:
        test_element = element
    xpath_str = \
        'preceding-sibling::text[' \
        'parent::para[' \
        'parent::*[self::subprov | self::label-para | self::def-para]' \
        ']]'
    has_preceding_sibling = bool(test_element.xpath(xpath_str))
    if has_preceding_sibling:
        # Depth one less than in subprov to correct indent
        depth = get_depth(element) - 1
        content, context = build_text(element, context)
        if context.mode == Mode.indd:
            return [E(f'section__{depth}', '\t', *content), '\n'], context
        elif context.mode == Mode.html:
            return [E.p(*content, {'class': f'para para__{depth}'})], context
    else:
        return [], context


@handler('history-note')
def historynote(element: ElementBase, context: Context) -> HandlerReturn:
    history_content = etree.tostring(
            element, method='text', encoding='utf8').decode('utf8').strip()
    if context.mode == Mode.indd:
        return [E.historynote(history_content), '\n'], context
    elif context.mode == Mode.html:
        return [
            E.p(history_content, {'class': 'para para__historynote'})
        ], context


################################################################################
# Inline element handlers                                                      #
################################################################################


@inline_handler('emphasis')
def emphasis(element: ElementBase, context: Context) -> HandlerReturn:
    style = element.get('style')
    if context.mode == Mode.indd:
        return [E(f'emphasis__{style}', _str(element.text))], context
    elif context.mode == Mode.html:
        if style == 'italic':
            return [E.em(_str(element.text))], context
        elif style == 'bold':
            return [E.strong(_str(element.text))], context
        elif style == 'bold-italic':
            return [E.strong(E.em(_str(element.text)))], context
        else:
            return [_str(element.text)], context


@inline_handler('citation')
def citation(element: ElementBase, context: Context) -> HandlerReturn:
    if context.mode == Mode.indd:
        return [*build_text(element, context)[0]], context
    elif context.mode == Mode.html:
        return [
            E.span(*build_text(element, context)[0], {'class': 'citation'})
        ], context


@inline_handler('extref')
def extref(element: ElementBase, context: Context) -> HandlerReturn:
    if context.mode == Mode.indd:
        return [E.extref(element.text)], context
    elif context.mode == Mode.html:
        id_ = element.get('href')
        if id_ is not None:
            url = f'http://prd-lgnz-nlb.prd.pco.net.nz/pdflink.aspx?id={id_}'
            return [E.a(
                _str(element.text),
                {'class': 'reference reference__external', 'href': url}
            )], context
        else:
            return [*build_text(element, context)[0]], context


@inline_handler('intref')
def intref(element: ElementBase, context: Context) -> HandlerReturn:
    if context.mode == Mode.indd:
        return [E.intref(element.text)], context
    elif context.mode == Mode.html:
        id_ = element.get('href')
        return [E.a(
            _str(element.text),
            {'class': 'reference reference__internal', 'href': f'#{id_}'}
        )], context


@inline_handler('quote.in')
def quote(element: ElementBase, context: Context) -> HandlerReturn:
    return [f'“{_str(element.text)}”'], context


@inline_handler('def-term')
def defterm(element: ElementBase, context: Context) -> HandlerReturn:
    if context.mode == Mode.indd:
        return [E.defined_term(_str(element.text))], context
    elif context.mode == Mode.html:
        return [E.span(_str(element.text), {'class': 'defterm'})], context


@inline_handler('leg-title')
def legtitle(element: ElementBase, context: Context) -> HandlerReturn:
    if context.mode == Mode.indd:
        return [E.legislation_title(_str(element.text))], context
    elif context.mode == Mode.html:
        return [E.span(_str(element.text), {'class': 'legtitle'})], context


@inline_handler('footnote')
def footnote(element: ElementBase, context: Context) -> HandlerReturn:
    footnote_content, context = build_text(element.find('./para/text'), context)
    context.current_footnote_number += 1
    index = context.current_footnote_number
    if context.mode == Mode.indd:
        label = f'__FOOTNOTE_{index}__'
        context.footnotes.extend([E.footnote(label, *footnote_content), '\n'])
        return [label], context
    elif context.mode == Mode.html:
        context.footnotes.extend([
            E.li(
                *footnote_content,
                ' ',
                E.a(
                    '↩',
                    href=f'#footnote_marker{index}'
                ),
                {'class': 'footnote__entry', 'id': f'footnote{index}'},
            )
        ])
        return [E.a(
            E.sup(
                f'[\u200A{index}\u200A]',
                id=f'footnote_marker{index}'),
            {'class': 'footnote__marker', 'href': f'#footnote{index}'}
        )], context


################################################################################
# Helper functions                                                             #
################################################################################


def _str(s: Any) -> str:
    if s is None:
        return ''
    else:
        return str(s)


def build_text(element: ElementBase, context: Context) -> HandlerReturn:
    initial_content = [_str(element.text)]
    for child in element.iterchildren():
        if child.tag in INLINE_HANDLERS:
            child_content, context = INLINE_HANDLERS[child.tag](child, context)
        else:
            child_content, context = build_text(child, context)
        initial_content.extend([*child_content, _str(child.tail)])
    merged_content = []
    for part in initial_content:
        if isinstance(part, str):
            if len(merged_content) > 0 and isinstance(merged_content[-1], str):
                merged_content[-1] += part
            else:
                if part != '':
                    merged_content.append(part)
        else:
            merged_content.append(part)
    return merged_content, context


def get_depth(element: ElementBase) -> int:
    xpath_str = 'ancestor::subprov | ' \
                'ancestor::label-para | ' \
                'ancestor::def-para'
    depth = len(element.xpath(xpath_str)) + 1
    if len(element.xpath('ancestor-or-self::def-para')) > 0 and depth > 1:
        depth -= 1
    return depth


def build_paragraph_reference(label_el: ElementBase) -> str:
    xpath_str = \
        'ancestor::*' \
        '[self::subprov | self::label-para | self::def-para]' \
        '/label[count(text()) > 0]'
    ref_parts = [el.text for el in label_el.xpath(xpath_str)]
    start_index = 0
    for n, ref_part in enumerate(ref_parts):
        if ref_part.count('.') > 1:
            start_index = n
    ref = '_'.join(rp for rp in ref_parts[start_index:])
    return ref


################################################################################
# Main functions                                                               #
################################################################################


def build_document_contents(input_file_path, context) -> HandlerReturn:
    with open(input_file_path, 'rb') as f:
        input_text = f.read()

    input_text = input_text.replace(b'\t', b'')
    root = etree.fromstring(input_text)
    schedule: ElementBase = root.find('.//schedule')
    document_contents = []
    for element in schedule.iter():
        if element.tag in HANDLERS:
            element_contents, context = HANDLERS[element.tag](element, context)
            document_contents.extend(element_contents)
    return document_contents, context


def build_html_table_of_contents(context: Context) -> Tuple[str, Context]:
    toc_contents = []
    for n, chapter_parts in enumerate(context.table_of_contents.items(), start=1):
        chapter_title, sectionheads = chapter_parts
        chapter_number = f'Chapter {n}: '
        sectionhead_elements = []
        for sectionhead_text, sectionhead_id in sectionheads:
            sectionhead_elements.append(
                E.li({'class': 'menu-section'},
                    E.a({'href': f'#{sectionhead_id}'}, sectionhead_text)
                )
            )
        if sectionhead_elements:
            sectionhead_elements = E.ol(
                {'class': 'menu-sections'},
                *sectionhead_elements)
            expand_button = E.span({'class': 'menu-chapter__expand-button'}, '')
        else:
            sectionhead_elements = ''
            expand_button = ''
        toc_contents.append(E.li({'class': 'menu-chapter'},
            E.a({'href': f'#chapter_{n}'},
                E.strong(chapter_number),
                chapter_title,
            ),
            expand_button,
            sectionhead_elements
        ))
    toc_els = E.ol(*toc_contents)
    toc = etree.tostring(
        toc_els,
        method='xml',
        encoding='unicode',
        pretty_print=True)
    return toc, context
