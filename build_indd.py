from lxml import etree
from lxml.builder import E

import clientcare

if __name__ == '__main__':
    document_contents, context = clientcare.build_document_contents(
        input_file_path='./document_src/Lawyers Conduct and Client Care Rules [formatted].xml',
        context=clientcare.Context(mode=clientcare.Mode.indd))

    document = E.root(*document_contents, *context.footnotes)
    content = etree.tostring(
        document,
        method='xml',
        encoding='unicode',
        pretty_print=True)
    with open('./indesign/clientcare.xml', 'w', encoding='utf8') as f:
        f.write(content)
